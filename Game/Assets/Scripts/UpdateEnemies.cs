﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEngine;
using SimpleJSON;
using System.Collections;

public class UpdateEnemies : MonoBehaviour
{
    public float shootForce;

    [SerializeField]
    private GameObject initialize;
    [SerializeField]
    private GameObject bullet;
    [SerializeField]
    private GameObject shotfireParticle;
    [SerializeField]
    private GameObject shotfirePoint;

    private Initialize init;

    // Start is called before the first frame update
    void Start()
    {
        init = initialize.GetComponent<Initialize>();
    }

    // Update is called once per frame
    void Update()
    {
        findPlayers();
    }

    private async Task findPlayers()
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add("match_id", PlayerPrefs.GetString("matchID"));
        data.Add("user_id", PlayerPrefs.GetString("userID"));

        var client = new HttpClient();
        var response = await client.PostAsync(ApiData.apiHost + "/game/find-players", new FormUrlEncodedContent(data));
        var content = await response.Content.ReadAsStringAsync();
        var json = JSON.Parse(content);
        if (json["code"] >= 400)
            return;

        var players = json["players"].AsStringArray;

        data = new Dictionary<string, string>();
        data.Add("match_id", PlayerPrefs.GetString("matchID"));
        data.Add("user_id", PlayerPrefs.GetString("userID"));
        client = new HttpClient();
        response = await client.PostAsync(ApiData.apiHost + "/game/get-player-data", new FormUrlEncodedContent(data));
        content = await response.Content.ReadAsStringAsync();
        json = JSON.Parse(content); 

        if (json["code"] < 300)
        {
            var _players = json["player_data"];
            foreach (var player in _players)
            {
                if (init.enemies.ContainsKey(player.Key))
                {
                    var playerComponent = init.enemies[player.Key].GetComponent<Player>();
                    // playerComponent.hp = float.Parse(player.Value["hp"]);
                    playerComponent.ammo = int.Parse(player.Value["ammo"]);
                    playerComponent.shooting = player.Value["shooting"].AsStringArray;
                    playerComponent.ID = player.Key;

                    Vector3 position = new Vector3(
                        float.Parse(player.Value["position"]["x"]),
                        float.Parse(player.Value["position"]["y"]),
                        float.Parse(player.Value["position"]["z"]));
                    Quaternion rotation = new Quaternion(
                        float.Parse(player.Value["rotation"]["x"]),
                        float.Parse(player.Value["rotation"]["y"]),
                        float.Parse(player.Value["rotation"]["z"]),
                        float.Parse(player.Value["rotation"]["w"]));

                    init.enemies[player.Key].transform.position = position;
                    init.enemies[player.Key].transform.rotation = rotation;
                }
                else
                {
                    Vector3 position = new Vector3(
                        float.Parse(player.Value["position"]["x"]),
                        float.Parse(player.Value["position"]["y"]),
                        float.Parse(player.Value["position"]["z"]));
                    Quaternion rotation = new Quaternion(
                        float.Parse(player.Value["rotation"]["x"]),
                        float.Parse(player.Value["rotation"]["y"]),
                        float.Parse(player.Value["rotation"]["z"]),
                        float.Parse(player.Value["rotation"]["w"]));
                    float hp = float.Parse(player.Value["hp"]);
                    int ammo = int.Parse(player.Value["ammo"]);
                    string[] shooting = player.Value["shooting"].AsStringArray;

                    GameObject enemy = Instantiate(init.enemyPrefab, position, rotation);
                    var playerComponent = enemy.GetComponent<Player>();

                    if (playerComponent.shooting.Length < shooting.Length)
                        shoot(shooting.Length - playerComponent.shooting.Length, player.Key);

                    playerComponent.hp = hp;
                    playerComponent.ammo = ammo;
                    playerComponent.shooting = shooting;
                    playerComponent.ID = player.Key;

                    init.enemies.Add(player.Key, enemy);
                }
            }
        }
    }

    /* private async Task<string[]> FindPlayers()
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add("match_id", PlayerPrefs.GetString("matchID"));
        data.Add("user_id", PlayerPrefs.GetString("userID"));

        var client = new HttpClient();
        var response = await client.PostAsync(ApiData.apiHost + "/game/find-players", new FormUrlEncodedContent(data));
        var content = await response.Content.ReadAsStringAsync();
        var json = JSON.Parse(content);

        if (json["code"] >= 400)
            return new string[0];

        return json["players"].AsStringArray;
    }

    private void DeleteExittedPlayers(string[] ids)
    {
        foreach(var enemy in init.enemies)
        {
            bool match = false;
            foreach(var id in ids)
            {
                if(enemy.Key.CompareTo(id) == 0)
                {
                    match = true;
                    break;
                }
            }

            if (!match)
                Destroy(init.enemies[enemy.Key]);
        }
    }

    private async Task UpdateEnemy()
    {
        var enemies = await FindPlayers();
        DeleteExittedPlayers(enemies);
        await UpdateEnemyParams();
    }

    private async Task UpdateEnemyParams()
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add("match_id", PlayerPrefs.GetString("matchID"));
        data.Add("user_id", PlayerPrefs.GetString("userID"));
        var client = new HttpClient();
        var response = await client.PostAsync(ApiData.apiHost + "/game/get-player-data", new FormUrlEncodedContent(data));
        var content = await response.Content.ReadAsStringAsync();
        var json = JSON.Parse(content);

        if (json["code"] < 300)
        {
            var players = json["player_data"];
            foreach (var player in players)
            {
                if (init.enemies.ContainsKey(player.Key))
                {
                    var playerComponent = init.enemies[player.Key].GetComponent<Player>();
                    playerComponent.hp = float.Parse(player.Value["hp"]);
                    playerComponent.ammo = int.Parse(player.Value["ammo"]);
                    playerComponent.shooting = player.Value["shooting"].AsStringArray;
                    playerComponent.ID = player.Key;

                    Vector3 position = new Vector3(
                        float.Parse(player.Value["position"]["x"]),
                        float.Parse(player.Value["position"]["y"]),
                        float.Parse(player.Value["position"]["z"]));
                    Quaternion rotation = new Quaternion(
                        float.Parse(player.Value["rotation"]["x"]),
                        float.Parse(player.Value["rotation"]["y"]),
                        float.Parse(player.Value["rotation"]["z"]),
                        float.Parse(player.Value["rotation"]["w"]));

                    init.enemies[player.Key].transform.position = position;
                    init.enemies[player.Key].transform.rotation = rotation;
                }
                else
                {
                    Vector3 position = new Vector3(
                        float.Parse(player.Value["position"]["x"]),
                        float.Parse(player.Value["position"]["y"]),
                        float.Parse(player.Value["position"]["z"]));
                    Quaternion rotation = new Quaternion(
                        float.Parse(player.Value["rotation"]["x"]),
                        float.Parse(player.Value["rotation"]["y"]),
                        float.Parse(player.Value["rotation"]["z"]),
                        float.Parse(player.Value["rotation"]["w"]));
                    float hp = float.Parse(player.Value["hp"]);
                    int ammo = int.Parse(player.Value["ammo"]);
                    string[] shooting = player.Value["shooting"].AsStringArray;

                    GameObject enemy = Instantiate(init.enemyPrefab, position, rotation);
                    var playerComponent = enemy.GetComponent<Player>();

                    if (playerComponent.shooting.Length < shooting.Length)
                        shoot(shooting.Length - playerComponent.shooting.Length, player.Key);

                    playerComponent.hp = hp;
                    playerComponent.ammo = ammo;
                    playerComponent.shooting = shooting;
                    playerComponent.ID = player.Key;

                    init.enemies.Add(player.Key, enemy);
                }
            }
        }
    } */

    private void shoot(int n, string player)
    {
        for(int i = 0; i < n; i++)
        {
            this.shotfirePoint = init.enemies[player].transform.Find("ShotFire").gameObject;
            var bullet = Instantiate(this.bullet, shotfirePoint.transform.position, Quaternion.identity);
            var bullet_rb = bullet.GetComponent<Rigidbody>();
            var shotfire = Instantiate(this.shotfireParticle, this.shotfirePoint.transform.position, Quaternion.identity, init.enemies[player].transform);
            StartCoroutine("destroyFire", shotfire);
            bullet_rb.AddForce(init.enemies[player].transform.forward * shootForce);
        }
    }

    IEnumerator destroyFire(GameObject shotfire)
    {
        yield return new WaitForSeconds(0.5f);
        Destroy(shotfire);
    }
}
