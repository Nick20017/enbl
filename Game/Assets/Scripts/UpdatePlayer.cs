﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEngine;
using SimpleJSON;

public class UpdatePlayer : MonoBehaviour
{
    private Player player;

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<Player>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        SendPlayerParams();
    }

    private async Task<bool> SendPlayerParams()
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add("user_id", PlayerPrefs.GetString("userID"));
        data.Add("match_id", PlayerPrefs.GetString("matchID"));
        data.Add("hp", player.hp.ToString());
        data.Add("ammo", player.ammo.ToString());
        data.Add("position_x", player.transform.position.x.ToString());
        data.Add("position_y", player.transform.position.y.ToString());
        data.Add("position_z", player.transform.position.z.ToString());
        data.Add("rotation_x", player.transform.rotation.x.ToString());
        data.Add("rotation_y", player.transform.rotation.y.ToString());
        data.Add("rotation_z", player.transform.rotation.z.ToString());
        data.Add("rotation_w", player.transform.rotation.w.ToString());
        Debug.Log(data);

        string shoots = "";
        foreach (string s in player.shooting)
            shoots += s + ",";
        shoots.TrimEnd(',');
        data.Add("shooting", shoots);

        var client = new HttpClient();
        var response = await client.PostAsync(ApiData.apiHost + "/game/update-player-data", new FormUrlEncodedContent(data));
        var content = await response.Content.ReadAsStringAsync();
        var json = JSON.Parse(content);
        Debug.Log(content);

        if (json["code"] >= 400)
            return false;
        else
            return true;
    }
}
