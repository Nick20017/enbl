﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchLogReg : MonoBehaviour
{
    [SerializeField]
    private Button signinButton;

    public void Switch()
    {
        var logreg = signinButton.GetComponent<Logreg>();
        logreg.isLogin = !logreg.isLogin;
    }
}
