﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class PlayerController : MonoBehaviour, IMovement, IShooting
{
    private Rigidbody rigibody;
    private Player player;

    [SerializeField]
    private Camera camera;
    [SerializeField]
    private Image aim;
    [SerializeField]
    private GameObject bullet;
    [SerializeField]
    private GameObject shotfirePoint;
    [SerializeField]
    private GameObject shotfireParticle;

    private bool isJumping = false;
    private float initialVelocity;
    private float runVelocity;

    public float velocity = 5f;
    public float rotationSpeed = 15f;
    public float jumpHeight = 2f;
    public float shootForce = 10000;

    // Start is called before the first frame update
    void Start()
    {
        rigibody = GetComponent<Rigidbody>();
        player = GetComponent<Player>();

        initialVelocity = velocity;
        runVelocity = velocity * 1.5f;
    }

    public void Rotate()
    {
        var mouseX = Input.GetAxis("Mouse X");
        var mouseY = Input.GetAxis("Mouse Y");
        transform.Rotate(0, mouseX * rotationSpeed * Time.deltaTime, 0);
        if(Mathf.Abs(camera.transform.rotation.x) < 30)
            camera.transform.Rotate(-mouseY * rotationSpeed * Time.deltaTime, 0, 0);
    }

    public void Jump()
    {
        if (!isJumping)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                rigibody.AddForce(transform.up * jumpHeight, ForceMode.Impulse);
                isJumping = true;
            }
        }
    }

    public void Move()
    {
        var x = Input.GetAxis("Horizontal");
        var z = Input.GetAxis("Vertical");
        var move = transform.right * x + transform.forward * z;

        if (Input.GetKey(KeyCode.LeftShift))
            velocity = runVelocity;
        else
            velocity = initialVelocity;

        rigibody.MovePosition(transform.position + move.normalized * velocity * Time.deltaTime);
    }

    public void Shoot()
    {
        if(Input.GetKey(KeyCode.Mouse0))
        {
            // var bullet = Instantiate(this.bullet, aim.transform.position, Quaternion.identity); 
            // var bullet_rb = bullet.GetComponent<Rigidbody>();
            var shotfire = Instantiate(this.shotfireParticle, this.shotfirePoint.transform.position, Quaternion.identity, camera.transform);
            StartCoroutine("destroyFire", shotfire);
            // bullet_rb.AddForce(camera.transform.forward * shootForce);
            RaycastHit hit;
            if(Physics.Raycast(aim.transform.position, transform.forward, out hit, 100f))
            {
                Debug.Log("Object was hit.\nObject tag: " + hit.collider.tag);
                if (hit.collider.tag.CompareTo("Enemy") == 0)
                {
                    hit.collider.gameObject.GetComponent<Player>().hp -= 10;
                    if (hit.collider.gameObject.GetComponent<Player>().hp <= 0)
                        Destroy(hit.collider.gameObject);
                }
            }

            Dictionary<string, string> data = new Dictionary<string, string>();

            data.Add("match_id", PlayerPrefs.GetString("matchID"));
            data.Add("user_id", PlayerPrefs.GetString("userID"));

            var client = new HttpClient();
            client.PostAsync(ApiData.apiHost + "/game/shoot", new FormUrlEncodedContent(data));
        }
    }

    IEnumerator destroyFire(GameObject shotfire)
    {
        yield return new WaitForSeconds(0.5f);
        Destroy(shotfire);
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.collider.tag.CompareTo("Ground") == 0)
            isJumping = false;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Jump();
        Rotate();
        Shoot();
    }
}
