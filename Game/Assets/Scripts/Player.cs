﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public string ID;
    public float hp = 100f;
    public int ammo = 120;
    public string[] shooting;
}
