﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Net.Http;
using SimpleJSON;
using System.Threading.Tasks;
using System;

public class Logreg : MonoBehaviour
{
    public bool isLogin = true;

    [SerializeField]
    private Text title;

    [SerializeField]
    private InputField username;

    [SerializeField]
    private InputField password;

    [SerializeField]
    private Text Switch;

    [SerializeField]
    private Text errorMessage;

    public void logreg()
    {
        signin();
    }

    private async Task<bool> signin()
    {
        try
        {
            var path = "";
            if (isLogin)
                path = "/login";
            else
                path = "/register";
            var dict = new Dictionary<string, string>();
            dict.Add("username", username.text);
            dict.Add("password", password.text);
            var httpClient = new HttpClient();
            var response = await httpClient.PostAsync(ApiData.apiHost + path, new FormUrlEncodedContent(dict));
            response.EnsureSuccessStatusCode();
            string content = await response.Content.ReadAsStringAsync();
            Debug.Log(content);
            JSONNode json = JSON.Parse(content);
            Debug.Log("code: " + json["code"]);
            Debug.Log("message: " + json["message"]);
            if (json["code"] == 200)
            {
                PlayerPrefs.SetString("userID", json["user_id"]);
                PlayerPrefs.SetString("username", username.text);
                PlayerPrefs.SetString("password", password.text);
                PlayerPrefs.Save();
                return true;
            }
            else
            {
                errorMessage.text = json["message"];
                return false;
            }
        } catch(Exception e)
        {
            errorMessage.text = e.Message;
            return false;
        }
    }

    public async Task<bool> SignIn(string username, string password)
    {
        this.username.text = username;
        this.password.text = password;

        var result = await this.signin();

        this.username.text = "";
        this.password.text = "";

        return result;
    }

    private void Update()
    {
        if (isLogin)
        {
            title.text = "E.N.B.L. - Sign in";
            Switch.text = "Don't have account yet? Click to register";
        }
        else
        {
            title.text = "E.N.B.L. - Register";
            Switch.text = "Already have account? Click to sign in";
        }
    }
}
