﻿using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System.Net.Http;
using System;

public class Initialize : MonoBehaviour
{
    public GameObject enemyPrefab;
    [SerializeField]
    private GameObject playerPrefab;
    [SerializeField]
    private GameObject spawns;
    [SerializeField]
    private GameObject[] spawnPoints;

    public Dictionary<string, GameObject> enemies;
    private GameObject player;

    private async void Awake()
    {
        var rand = new System.Random(DateTime.Now.Millisecond);
        List<GameObject> spawns = new List<GameObject>();
        for (var i = 0; i < this.spawns.transform.childCount; i++)
            spawns.Add(this.spawns.transform.GetChild(i).gameObject);
        this.spawnPoints = spawns.ToArray();
        this.player = Instantiate(playerPrefab, spawnPoints[rand.Next(0, spawnPoints.Length)].transform.position, Quaternion.identity);
    
        enemies = new Dictionary<string, GameObject>();

        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add("match_id", PlayerPrefs.GetString("matchID"));
        data.Add("user_id", PlayerPrefs.GetString("userID"));

        var player = this.player.GetComponent<Player>();
        data.Add("hp", player.hp.ToString());
        data.Add("ammo", player.ammo.ToString());
        data.Add("position_x", this.player.transform.position.x.ToString());
        data.Add("position_y", this.player.transform.position.y.ToString());
        data.Add("position_z", this.player.transform.position.z.ToString());
        data.Add("rotation_x", this.player.transform.rotation.x.ToString());
        data.Add("rotation_y", this.player.transform.rotation.y.ToString());
        data.Add("rotation_z", this.player.transform.rotation.z.ToString());
        data.Add("rotation_w", this.player.transform.rotation.w.ToString());

        var client = new HttpClient();
        var response = await client.PostAsync(ApiData.apiHost + "/game/join-match", new FormUrlEncodedContent(data));
        var content = await response.Content.ReadAsStringAsync();
        var json = JSON.Parse(content);

        if (json["code"] < 300)
        {
            foreach (var p in json["players"])
            {
                if (!enemies.ContainsKey(p.Key))
                {
                    Vector3 position = new Vector3(
                    float.Parse(p.Value["position"]["x"]),
                    float.Parse(p.Value["position"]["y"]),
                    float.Parse(p.Value["position"]["z"]));
                    Quaternion rotation = new Quaternion(
                        float.Parse(p.Value["rotation"]["x"]),
                        float.Parse(p.Value["rotation"]["y"]),
                        float.Parse(p.Value["rotation"]["z"]),
                        float.Parse(p.Value["rotation"]["w"]));
                    float hp = float.Parse(p.Value["hp"]);
                    int ammo = int.Parse(p.Value["ammo"]);
                    string[] shooting = p.Value["shooting"].AsStringArray;

                    GameObject enemy = Instantiate(enemyPrefab, position, rotation);
                    var playerComponent = enemy.GetComponent<Player>();
                    playerComponent.hp = hp;
                    playerComponent.ammo = ammo;
                    playerComponent.shooting = shooting;

                    enemies.Add(p.Key, enemy);
                }
            }
        }
    }
}
