﻿using System.Collections.Generic;
using System.Net.Http;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitMatch : MonoBehaviour
{
    public async void LeaveMatch()
    {
        var data = new Dictionary<string, string>();
        data.Add("user_id", PlayerPrefs.GetString("userID"));
        data.Add("match_id", PlayerPrefs.GetString("matchID"));
        var client = new HttpClient();
        await client.PostAsync(ApiData.apiHost + "/game/exit-match", new FormUrlEncodedContent(data));
        PlayerPrefs.DeleteKey("matchID");
        SceneManager.LoadScene(0);
    }
}
