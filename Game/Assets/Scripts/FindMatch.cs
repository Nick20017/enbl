﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using UnityEngine;
using SimpleJSON;
using UnityEngine.UI;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;

public class FindMatch : MonoBehaviour
{
    [SerializeField]
    private Text errorMessage;

    public async void JoinMatch(bool singleplayer)
    {
        if (singleplayer) { }
        else
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("user_id", PlayerPrefs.GetString("userID"));
            HttpClient client = new HttpClient();
            var response = await client.PostAsync(ApiData.apiHost + "/game/find-match", new FormUrlEncodedContent(data));
            var resObject = await response.Content.ReadAsStringAsync();
            var json = JSONNode.Parse(resObject);
            if (json["code"] >= 400)
            {
                errorMessage.text = json["message"];
                return;
            }
            PlayerPrefs.SetString("matchID", json["match_id"]);
            PlayerPrefs.Save();
            SceneManager.LoadSceneAsync(1);
        }
    }
}
