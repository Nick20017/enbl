﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateUI : MonoBehaviour
{
    private Player player;
    [SerializeField]
    private Text hp;
    [SerializeField]
    private Text ammo;

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        hp.text = player.hp.ToString();
        ammo.text = player.ammo.ToString();
    }
}
