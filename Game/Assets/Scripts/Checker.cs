﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checker : MonoBehaviour
{
    public bool isLogreg = true;
    [SerializeField]
    private GameObject logreg;

    public bool isMainMenu = false;
    [SerializeField]
    private GameObject mainMenu;
    // Start is called before the first frame update
    async void Start()
    {
        if(
            !PlayerPrefs.HasKey("userID")
            || !PlayerPrefs.HasKey("username")
            || !PlayerPrefs.HasKey("password"))
        {
            isLogreg = true;
            isMainMenu = false;
        }
        else
        {
            var lr = logreg.GetComponent<Logreg>();
            var isLoggedIn = await lr.SignIn(PlayerPrefs.GetString("username"), PlayerPrefs.GetString("password"));
            if(isLoggedIn)
            {
                isLogreg = false;
                isMainMenu = true;
            }
            else
            {
                isLogreg = true;
                isMainMenu = false;
            }
        }
    }

    // Update is called once per frame 
    void Update()
    {
        if (isLogreg)
            logreg.SetActive(true);
        else
            logreg.SetActive(false);

        if (isMainMenu)
            mainMenu.SetActive(true);
        else
            mainMenu.SetActive(false);
    }
}
