﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collision detected");
        if (collision.collider.tag.CompareTo("Player") == 0)
            collision.collider.gameObject.GetComponent<Player>().hp -= 10;
        else if (collision.collider.tag.CompareTo("Enemy") == 0)
        {
            collision.collider.gameObject.GetComponent<Player>().hp -= 10;
            var player = collision.collider.gameObject.GetComponent<Player>();
            if (player.hp <= 0)
                Destroy(collision.collider.gameObject);
        }

        Destroy(this.gameObject);
    }
}
