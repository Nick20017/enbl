package com.minemodsgames.enbl.activity;

import kong.unirest.Unirest;

import java.util.ArrayList;

public class Activity {
    private static final String apiHost = "http://enbl-server.herokuapp.com";

    public String userID;
    public String username;
    public String text;
    public String date;
    public String time;
    public String type;
    public String postID;

    public Activity(String userID, String username, String text, String date, String time, String type, String postID) {
        this.userID = userID;
        this.username = username;
        this.text = text;
        this.date = date;
        this.time = time;
        this.type = type;
        if (type.compareTo("post") == 0)
            this.postID = postID;
        else
            this.postID = null;
    }

    public static boolean addActivity(String senderID, String targetID, String type) {
        var response = Unirest.post(apiHost + "/profile/add-activity")
                .field("sender_id", senderID)
                .field("target_id", targetID)
                .field("type", type)
                .asJson();
        var json = response.getBody().getObject();
        if(json.has("code")) {
            if (json.getInt("code") >= 400)
                return false;
            else
                return true;
        }

        return false;
    }

    public static Activity[] getActivities(String userID) {
        var response = Unirest.post(apiHost + "/profile/get-activities")
                .field("user_id", userID)
                .asJson();

        var json = response.getBody().getObject();
        System.out.println(json);

        if (json.has("code")) {
            if(json.getInt("code") >= 400)
                return new Activity[0];
        } else
            return new Activity[0];

        var activities = json.getJSONArray("activities");
        var actions = new ArrayList<Activity>();
        for (var i = 0; i < activities.length(); i++) {
            var act = activities.getJSONObject(i);
            Activity activity = new Activity(
                    act.getString("user_id"),
                    act.getString("username"),
                    act.getString("text"),
                    act.getString("date"),
                    act.getString("time"),
                    act.getString("type"),
                    act.getString("type").compareTo("post") == 0 ? act.getString("post_id") : null
            );
            actions.add(activity);
        }

        return actions.toArray(new Activity[0]);
    }
}
