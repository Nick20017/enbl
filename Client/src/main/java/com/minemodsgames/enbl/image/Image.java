package com.minemodsgames.enbl.image;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

public class Image {
    private static final String apiHost = "http://enbl-server.herokuapp.com";

    public static ResponseEntity<byte[]> retrieveImage(String id, String extension, String type) {
        HttpResponse<JsonNode> response;
        if (type.compareTo("post") == 0)
            response = Unirest.post(apiHost + "/post/get-bytes")
                    .field("post_id", id)
                    .asJson();
        else
            response = Unirest.post(apiHost + "/profile/get-profile-image-bytes")
                    .field("user_id", id)
                    .asJson();
        var object = response.getBody().getObject();
        if (object.has("code")) {
            if (object.getInt("code") >= 400)
                return ResponseEntity.badRequest().body(new byte[0]);
        } else {
            return ResponseEntity.badRequest().body(new byte[0]);
        }
        if (!object.has("bytes")) {
            return ResponseEntity.badRequest().body(new byte[0]);
        }
        var bytesString = object.getString("bytes");
        String[] bytesStr = bytesString.split(",");
        byte[] bytes = new byte[bytesStr.length];

        for(var i = 0; i < bytesStr.length; i++) {
            bytes[i] = (byte)Integer.parseInt(bytesStr[i]);
        }

        MediaType mediaType = extension.compareTo("png") == 0 ? MediaType.IMAGE_PNG : extension.compareTo("jpg") == 0 || extension.compareTo("jpeg") == 0 ? MediaType.IMAGE_JPEG : MediaType.IMAGE_GIF;
        return ResponseEntity.ok().contentType(mediaType).body(bytes);
    }
}
