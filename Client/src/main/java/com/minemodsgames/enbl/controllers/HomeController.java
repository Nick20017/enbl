package com.minemodsgames.enbl.controllers;

import com.minemodsgames.enbl.activity.Activity;
import com.minemodsgames.enbl.image.Image;
import com.minemodsgames.enbl.messaging.Messaging;
import com.minemodsgames.enbl.user.Post;
import com.minemodsgames.enbl.user.User;
import kong.unirest.Unirest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.Deflater;

@Controller
public class HomeController {
    // Constants
    private final String apiHost = "http://enbl-server.herokuapp.com";

    // Get requests
    @GetMapping("/")
    public String index(Model model) {
        return "index";
    }

    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("Title", "Login");
        model.addAttribute("PageIndex", -1);
        model.addAttribute("logregID", 0);
        return "login";
    }

    @GetMapping("/myaccount")
    public RedirectView myaccount(@CookieValue(value="userID", defaultValue="") String userID) {
        return new RedirectView("/account?id=" + userID);
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("Title", "Register"); // Set page title
        model.addAttribute("PageIndex", -1); // Set page index for menu display
        model.addAttribute("logregID", 1); // Set ID that specifies if it's login page or register
        return "register";
    }

    @GetMapping("/game")
    public String game(Model model) {
        return "game/index";
    }

    @GetMapping("/account")
    public String account(
            @RequestParam String id,
            @CookieValue(value="userID", defaultValue = "") String userID,
            Model model) {
        User user = User.getProfile(id);
        model.addAttribute("user", user);
        assert user != null;
        model.addAttribute("Title", user.username);
        model.addAttribute("userID", id);
        model.addAttribute("IsMyAccount", id.compareTo(userID) == 0 ? 1 : 0);
        model.addAttribute("IsFollowing", User.isFollowing(userID, id));
        model.addAttribute("PageIndex", id.compareTo(userID) == 0 ? 2 : 10);
        return "account";
    }

    @GetMapping("/find")
    public String findUser(Model model) {
        model.addAttribute("Title", "Find user");
        model.addAttribute("PageIndex", 3);
        return "finduser";
    }

    @GetMapping("/followers")
    public String followers(
            @RequestParam String id,
            Model model
    ) {
        var followers = User.getFollows(id, "followers");
        model.addAttribute("Title", "Followers");
        model.addAttribute("PageIndex", 7);
        model.addAttribute("previousPage", "/account?id=" + id);
        model.addAttribute("users", followers);
        return "follows";
    }

    @GetMapping("/followings")
    public String followings(
            @RequestParam String id,
            Model model
    ) {
        var followings = User.getFollows(id, "followings");
        model.addAttribute("Title", "Followings");
        model.addAttribute("PageIndex", 7);
        model.addAttribute("previousPage", "/account?id=" + id);
        model.addAttribute("users", followings);
        return "follows";
    }

    @GetMapping("/post/upload")
    public String uploadPost(
            @CookieValue(value = "userID", defaultValue = "") String userID,
            Model model
    ) {
        model.addAttribute("Title", "Upload post");
        model.addAttribute("PageIndex", 4);
        return "uploadpost";
    }

    @GetMapping("/activity")
    public String activity(
            @CookieValue(value="userID", defaultValue = "") String userID,
            Model model) {
        model.addAttribute("Title", "Activity");
        model.addAttribute("PageIndex", 5);
        model.addAttribute("activities", Activity.getActivities(userID));

        return "activity";
    }

    @GetMapping("/messaging")
    public String messaging(
            @CookieValue(value = "userID") String userID,
            @RequestParam(required=false) String id,
            Model model) {
        model.addAttribute("Title", "Messaging");
        model.addAttribute("PageIndex", 0);
        model.addAttribute("CurrentUser", userID);
        model.addAttribute("TargetUser", id != null ? id : 0);

        return "messaging";
    }

    // Post requests
    @PostMapping("/login")
    @ResponseBody
    public String login(@RequestParam String login, @RequestParam String password) throws IOException {
        var response = Unirest.post(apiHost + "/login")
                .field("username", login)
                .field("password", password)
                .asString();
        return response.getBody();
    }

    @PostMapping("/register")
    @ResponseBody
    public String register(@RequestParam String login, @RequestParam String password) throws IOException {
        Resource resource = new ClassPathResource("static/img/profile/profile-picture.jpg");
        var profileImage = resource.getInputStream();
        var bytes = profileImage.readAllBytes();
        var bytesString = "";
        for (var i = 0; i < bytes.length; i++)
            bytesString += bytes[i] + ",";

        var response = Unirest.post(apiHost + "/register")
                .field("username", login)
                .field("password", password)
                .field("profile_image", bytesString)
                .asString();
        return response.getBody();
    }

    @PostMapping("/cookies/set-cookie")
    @ResponseBody
    public String setCookie(@RequestParam String[] data, HttpServletResponse response) {
        try {
            int lastIndex = data.length % 2 == 0 ? data.length : data.length - 1;
            for (int i = 0; i < lastIndex; i += 2) {
                Cookie cookie = new Cookie(data[i], data[i + 1]);
                cookie.setMaxAge(365 * 24 * 60 * 60);
                cookie.setPath("/");
                response.addCookie(cookie);
            }
            return "1";
        } catch (Exception e) {
            return "0";
        }
    }

    @PostMapping("/profile/follow")
    @ResponseBody
    public String profileFollow(
            @RequestParam String followingID,
            @CookieValue(value = "userID", defaultValue = "") String userID) {
        Activity.addActivity(userID, followingID, "follow");
        return User.follow(userID, followingID);
    }

    @PostMapping("/profile/unfollow")
    @ResponseBody
    public String profileUnfollow(@RequestParam String followingID, @CookieValue(value = "userID", defaultValue = "") String userID) {
        return User.unfollow(userID, followingID);
    }

    @PostMapping("/profile/get-profiles-by-username")
    @ResponseBody
    public String getProfilesByUsername(@RequestParam String username) {
        var response = User.getProfilesByUsername(username);
        return response;
    }
    
    @PostMapping("/post/upload")
    @ResponseBody
    public String UploadPost(
            @CookieValue(value = "userID", defaultValue = "") String userID,
            @RequestParam MultipartFile picture,
            @RequestParam String description) throws IOException {
        var bytes = picture.getBytes();
        var bytes_str = "";
        for (var i = 0; i < bytes.length; i++)
            bytes_str += bytes[i] + ",";

        var filename = picture.getOriginalFilename().toLowerCase().split(".");

        var response = Unirest.post(apiHost + "/post/upload")
                .field("user_id", userID)
                .field("image", bytes_str)
                .field("extension", filename.length > 0 ? filename[filename.length - 1] : "png")
                .field("description", description)
                .asString();

        return response.getBody();
    }

    @PostMapping("/messaging/send-message")
    @ResponseBody
    public String SendMessage(
            @RequestParam String senderID,
            @RequestParam String targetID,
            @RequestParam String message
    ) {
        return Messaging.sendMessage(senderID, targetID, message);
    }

    @PostMapping("/messaging/send-image")
    @ResponseBody
    public String SendImage(
            @RequestParam String senderID,
            @RequestParam String targetID,
            @RequestParam MultipartFile image
    ) throws IOException {
        return Messaging.sendMessage(senderID, targetID, image);
    }

    @PostMapping("/messaging/get-chat-previews")
    @ResponseBody
    public String GetChatPreviews(
            @RequestParam String userID
    ) {
        return Messaging.getChatPreviews(userID);
    }

    @PostMapping("/messaging/get-chat")
    @ResponseBody
    public String GetChat(
            @RequestParam String currentUserID,
            @RequestParam String targetUserID
    ) {
        return Messaging.getChat(currentUserID, targetUserID);
    }

    @GetMapping("/retrieve-image")
    public ResponseEntity<byte[]> retrieveImage(
            @RequestParam String id,
            @RequestParam String extension,
            @RequestParam String type) {
        return Image.retrieveImage(id, extension, type);
    }

    // Testing
    @GetMapping("/get-profile")
    @ResponseBody
    private String getProfile() {
        return Unirest.post(apiHost + "/get-profile").field("user_id", "r46123meo").asString().getBody();
    }

    @GetMapping("/get-profiles")
    @ResponseBody
    private String getProfiles() {
        return Unirest.post(apiHost + "/get-profiles-by-username").field("username", "rom1").asString().getBody();
    }
}