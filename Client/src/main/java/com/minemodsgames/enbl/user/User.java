package com.minemodsgames.enbl.user;

import kong.unirest.Unirest;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONObject;

import java.util.ArrayList;

public class User {
    // API HOST
    private static final String apiHost = "http://enbl-server.herokuapp.com";

    public String userID;
    public String username;
    public String profileImage;
    public Post[] posts;
    public int numPosts;
    public int numFollowers;
    public int numFollowings;

    public User(String userID, String username, String profileImage, Post[] posts, int numFollowers, int numFollowings) {
        this.userID = userID;
        this.username = username;
        this.profileImage = profileImage;
        this.posts = posts;
        this.numPosts = posts.length;
        this.numFollowers = numFollowers;
        this.numFollowings = numFollowings;
    }

    public static User getProfile(String userID) {
        var response = Unirest.post(apiHost + "/get-profile")
                .field("user_id", userID)
                .asJson();
        var object = response.getBody().getObject();

        if (object.has("code"))
            if (object.getInt("code") >= 400)
                return null;

        ArrayList<Post> posts = new ArrayList<Post>();
        var postsObject = object.has("posts") ? object.getJSONArray("posts") : new JSONArray();

        for(int i = 0; i < postsObject.length(); i++) {
            var post = postsObject.getJSONObject(i);
            posts.add(new Post(
                    post.has("user_id") ? post.getString("user_id") : "",
                    post.has("post_id") ? post.getString("post_id") : "",
                    post.has("extension") ? post.getString("extension") : "png",
                    post.has("description") ? post.getString("description") : "",
                    post.has("created_date") ? post.getString("created_date") : "",
                    post.has("created_time") ? post.getString("created_time") : ""
            ));
        }

        var user = new User(
                object.has("user_id") ? object.getString("user_id") : "",
                object.has("username") ? object.getString("username") : "",
                object.has("profile_image") ? object.getString("profile_image") : "",
                posts.toArray(new Post[0]),
                object.has("num_followers") ? object.getInt("num_followers") : 0,
                object.has("num_followings") ? object.getInt("num_followings") : 0
        );

        return user;
    }

    public static String getProfilesByUsername(String username) {
        var response = Unirest.post(apiHost + "/get-profiles-by-username")
                .field("username", username)
                .asString();
        return response.getBody();
    }

    public static String follow(String follower, String following) {
        var response = Unirest.post(apiHost + "/profile/follow")
                .field("follower", follower)
                .field("following", following)
                .asJson();
        var object = response.getBody().getObject();
        return String.valueOf(object.has("code") ? object.getInt("code") : 400);
    }

    public static String unfollow(String unfollower, String unfollowing) {
        var response = Unirest.post(apiHost + "/profile/unfollow")
                .field("unfollower", unfollower)
                .field("unfollowing", unfollowing)
                .asJson();
        var object = response.getBody().getObject();
        return String.valueOf(object.has("code") ? object.getInt("code") : 400);
    }

    public static int isFollowing(String currentUserID, String targetUserID) {
        var response = Unirest.post(apiHost + "/profile/is-following")
                .field("current_user_id", currentUserID)
                .field("target_user_id", targetUserID)
                .asJson();

        var object = response.getBody().getObject();
        if (object.has("code")) {
            if(object.getInt("code") >= 400)
                return 0;
            else {
                if(object.has("is_following"))
                    return object.getInt("is_following");
                else
                    return 0;
            }
        }
        return 0;
    }

    public static User[] getFollows(String userID, String follows) {
        var response = Unirest.post(apiHost + "/profile/get-follows")
                .field("user_id", userID)
                .field("follows", follows)
                .asJson();

        var object = response.getBody().getObject();
        if (object.has("code")) {
            if (object.getInt("code") == 400)
                return new User[0];
        } else {
            return new User[0];
        }

        var users = new ArrayList<User>();
        var usersObject = object.getJSONArray("users");
        for (var i = 0; i < usersObject.length(); i++) {
            var userObject = usersObject.getJSONObject(i);
            if (!userObject.has("user_id"))
                continue;
            if(!userObject.has("username"))
                continue;
            users.add(new User(
                    userObject.getString("user_id"),
                    userObject.getString("username"),
                    userObject.has("profile_image") ? userObject.getString("profile_image") : "",
                    new Post[0],
                    0,
                    0
            ));
        }

        return users.toArray(new User[0]);
    }
}
