package com.minemodsgames.enbl.user;

public class Post {
    public String userID;
    public String postID;
    public String extension;
    public String description;
    public String createdDate;
    public String createdTime;

    public Post(String userID, String postID, String extension, String description, String createdDate, String createdTime) {
        this.userID = userID;
        this.postID = postID;
        this.extension = extension;
        this.description = description;
        this.createdDate = createdDate;
        this.createdTime = createdTime;
    }
}
