package com.minemodsgames.enbl.messaging;

import kong.unirest.Unirest;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public class Messaging {
    private static final String apiHost = "http://enbl-server.herokuapp.com";

    public static String sendMessage(String senderID, String targetID, String message) {
        var response = Unirest.post(apiHost + "/messaging/send-message")
                .field("sender_id", senderID)
                .field("target_id", targetID)
                .field("message", message)
                .field("type", "text")
                .asString();
        return response.getBody();
    }

    public static String sendMessage(String senderID, String targetID, MultipartFile image) throws IOException {
        var imageBytes = image.getBytes();
        var imageBytesString = "";
        for (var i = 0; i < imageBytes.length; i++) {
            imageBytesString += imageBytes[i] + ",";
        }

        var response = Unirest.post(apiHost + "/messaging/send-message")
                .field("sender_id", senderID)
                .field("target_id", targetID)
                .field("message", imageBytesString)
                .field("type", "image")
                .asString();
        return response.getBody();
    }

    public static String getChatPreviews(String userID) {
        var response = Unirest.post(apiHost + "/messaging/get-chat-previews")
                .field("user_id", userID)
                .asString();
        return response.getBody();
    }

    public static String getChat(String currentUserID, String targetUserID) {
        var response = Unirest.post(apiHost + "/messaging/get-chat")
                .field("current_user_id", currentUserID)
                .field("target_user_id", targetUserID)
                .asString();
        System.out.println(response.getBody());
        return response.getBody();
    }
}
