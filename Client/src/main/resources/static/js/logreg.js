var content = new Vue({
    el: '#content',
    data: {
        logregID: document.getElementById('logregID').innerText,
        username: document.getElementById('username').value,
        password: document.getElementById('password').value
    },
    methods: {
        push: function() {
            var url = null;
            if (this.logregID == 0) {
                url = '/login'
                document.getElementById('error').innerText = "Logging in...";
            }
            else {
                url = '/register'
                document.getElementById('error').innerText = "Signing up...";
            }
            
            fetch(url, {
                method: 'POST',
                mode: 'same-origin',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: 'login=' + this.username + '&password=' + this.password
            }).then((response) => {
                return response.json();
            }, (reason) => {
                document.getElementById('error').innerText = reason;
            }).then((data) => {
                if (data.code == 200) {
                    if(setCookie(data.user_id) == 1) {
                        window.location.replace('/');
                    } else {
                        document.getElementById('error').innerText = "Couldn't connect to server!"
                    }
                } else {
                    document.getElementById('error').innerText = data.message;
                }
            }, (reason) => {
                document.getElementById('error').innerText = reason;
            });
        },
        inputUsername: function(value) {
            this.username = value;
        },
        inputPassword: function(value) {
            this.password = value;
        }
    }
});

function setCookie(userID) {
    try {
        var xhttp = null;
        if (window.XMLHttpRequest)
            xhttp = new XMLHttpRequest();
        else
            xhttp = new ActiveXObject('Microsoft.XMLHTTP');

        xhttp.onerror = function() {
            return JSON.parse('0');
        }

        xhttp.open('POST', '/cookies/set-cookie', false);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("data=userID," + userID + ",username," + document.getElementById('username').value + ",password," + document.getElementById('password').value);
        return xhttp.responseText;
    } catch(ex) { 
        return 0;
    }
}