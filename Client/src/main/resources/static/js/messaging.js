var content = new Vue({
    el: '#content',
    data: {
        chatPreviews: [],
        messages: [],
        emojis: [],
        targetUsername: '',
        message: '',
        senderID: document.getElementById('current-user').innerText,
        targetID: document.getElementById('target-user').innerText,
        isChatSelected: document.getElementById('target-user').innerText != 0,
        isSending: false
    },
    methods: {
        messageInput(value) {
            this.message = value;
        },
        selectChat(id) {
            window.location.replace('/messaging?id=' + id);
        },
        sendMessage() {
            if (this.message == '')
                return null;

            this.isSending = true;

            console.log("Sending message: " + this.message);
            fetch('/messaging/send-message', {
                method: 'POST',
                mode: 'same-origin',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: 'senderID=' + this.senderID + '&targetID=' + this.targetID + '&message=' + this.message
            }).then((response) => {
                console.log("Returning response as json")
                return response.json();
            }).then((data) => {
                console.log(JSON.stringify(data));
                if (data.code >= 400) {
                    console.log(data.message)
                    return null;
                }
            });

            this.message = '';
            document.getElementById('input').value = '';
        },
        getChatPreviews() {
            console.log("Getting chat previews...");
            fetch('/messaging/get-chat-previews', {
                method: 'POST',
                mode: 'same-origin',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: 'userID=' + this.senderID
            }).then((response) => {
                console.log("Returning response as json")
                return response.json();
            }).then((data) => {
                console.log(JSON.stringify(data));
                if (data.code >= 400) {
                    console.log(data.message);
                    return null;
                }

                this.chatPreviews = data.previews;

                console.log(data.previews);
            });
        },
        getChat() {
            if(!this.isChatSelected)
                return null;
            console.log("Getting chat...");
            fetch('/messaging/get-chat', {
                method: 'POST',
                mode: 'same-origin',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: 'currentUserID=' + this.senderID + '&targetUserID=' + this.targetID
            }).then((response) => {
                console.log("Returning response as json")
                return response.json();
            }).then((data) => {
                console.log(JSON.stringify(data));
                if (data.code >= 400) {
                    console.log(data.message)
                    return null;
                }

                this.targetUsername = data.username;
                this.messages = data.chat;

                console.log(data.chat);
            });
        }
    },
    beforeMount() {
        console.log("isChatSelected: " + this.isChatSelected);
        console.log("targetID: " + this.targetID);
        setInterval(this.getChatPreviews, 1000);
        setInterval(this.getChat, 1000);
    }
});