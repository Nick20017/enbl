Vue.component('previous-page', {
    data() {
        return {
            previousPage: document.getElementById("previous-page") ? document.getElementById("previous-page").innerText : ""
        }
    },
    template: '<div><div id="previouspage" class="header-item"><a :href="previousPage"><img src="/img/header/arrow.png" /></a></div></div>'
});

Vue.component('header-logo', {
    template: '<div>'
    + '<div class="header-item">'
    + '<img src="/img/header/logo.png" id="header-logo" />'
    + '</div>'
    + '</div>'
});

Vue.component('header-title', {
    data() {
        return {
            title: document.getElementById('hidden-title').innerText
        };
    },
    template: '<div><div id="header-title" class="header-item">E.N.B.L. - {{ title }}</div></div>'
});

Vue.component('header-menubar-icon', {
    template: '<div><div class="header-item" id="menuBar"><img src="/img/header/menu-bar-icon.png" id="header-menu" /></div></div>'
});

Vue.component('header-menubar-list', {
    data() {
        return {
            pageIndex: document.getElementById('page-index').innerText
        }
    },
    template: '<div>'
    + '<div id="menubar-list">'
    + '<ul>'
    + '<li v-if="pageIndex==0" class="menubar-list-item" id="selected">Messaging</li>'
    + '<li v-else class="menubar-list-item"><a href="/messaging">Messaging</a></li>'
    + '<li v-if="pageIndex==1" class="menubar-list-item" id="selected">Feed</li>'
    + '<li v-else class="menubar-list-item"><a href="/">Feed</a></li>'
    + '<li v-if="pageIndex==2" class="menubar-list-item" id="selected">My account</li>'
    + '<li v-else class="menubar-list-item"><a href="/myaccount">My account</a></li>'
    + '<li v-if="pageIndex==3" class="menubar-list-item" id="selected">Find user</li>'
    + '<li v-else class="menubar-list-item"><a href="/find">Find user</a></li>'
    + '<li v-if="pageIndex==4" class="menubar-list-item" id="selected">Upload post</li>'
    + '<li v-else class="menubar-list-item"><a href="/post/upload">Upload post</a></li>'
    + '<li v-if="pageIndex==5" class="menubar-list-item" id="selected">Activity</li>'
    + '<li v-else class="menubar-list-item"><a href="/activity">Activity</a></li>'
    + '<li v-if="pageIndex==6" class="menubar-list-item" id="selected">Settings</li>'
    + '<li v-else class="menubar-list-item"><a href="/settings">Settings</a></li>'
    + '<li class="menubar-list-item"><a href="/login">Log out</a></li>'
    + '</ul>'
    + '</div>'
    + '</div>'
});

Vue.component('header-menubar', {
    data() {
        return {
            isMenuBarShown: false,
            pageIndex: document.getElementById('page-index').innerText
        }
    },
    methods: {
        showMenu() {
            this.isMenuBarShown = !this.isMenuBarShown;
        }
    },
    template: '<div><header-menubar-icon @click.native="showMenu" v-if="pageIndex >= 0"></header-menubar-icon><header-menubar-list v-if="isMenuBarShown"></header-menubar-list></div>'
})

Vue.component('header-block', {
    template: '<div><div id="header-div"><header-logo></header-logo><header-title></header-title><header-menubar></header-menubar></div></div>'
});

var header = new Vue({
    el: '#header',
    data: {
    },
    methods: {

    }
});

previousPage = document.getElementById("previous-page")
if(previousPage) {
    var headerDiv = document.getElementById("header");
    headerDiv.style.display = "grid";
    headerDiv.style.gridTemplateColumns = "100px auto";
    headerDiv.style.gridColumnGap = "10px";
}