var content = new Vue({
    el: '#content',
    data: {
        isLoading: false,
        users: [],
        status: ''
    },
    methods: {
        find(value) {
            this.isLoading = true;
            this.users = [];

            this.status = "Loading...";

            fetch('/profile/get-profiles-by-username', {
                method: 'POST',
                mode: 'same-origin',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: 'username=' + value
            }).then((response) => {
                return response.json();
            }, (reason) => {
                this.status = reason;
            }).then((data) => {
                if(data.code >= 400) {
                    this.status = data.message;
                    return;
                }

                this.users = data.users;

                if(this.users.length == 0)
                    this.status = "User not found";
                else
                    this.isLoading = false;
            }, (reason) => {
                this.status = reason;
            });
        }
    }
})