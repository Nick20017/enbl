var content = new Vue({
    el: '#content',
    data: {
        isMyAccount: document.getElementById('is-my-account').innerText == 0 ? false : true,
        isFollowing: document.getElementById('is-following').innerText == 0 ? false : true,
        userID: document.getElementById('hidden-userID').innerText
    },
    methods: {
        follow: function() {
            this.isFollowing = !this.isFollowing;
            var url = "unfollow";

            if(this.isFollowing) {
                url = "follow";
            }

            fetch('/profile/' + url, {
                method: 'POST',
                mode: 'same-origin',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: 'followingID=' + this.userID
            }).then((response) => {
                return response.json();
            }).then((data) => {
                if (data.code >= 400)
                    this.isFollowing = !this.isFollowing;
            });
        }
    }
});