var content = new Vue({
    el: '#content',
    data: {
        picture: '/img/header/logo.png',
        extensions: '.png, .gif, .jpg, .jpeg, .webp, .bmp, .svg, .svgz'
    },
    methods: {
        openDialog() {
            var fileDialog = document.getElementById("picture");
            fileDialog.click();
        },
        checkExtension(e) {
            var fileDialog = document.getElementById("picture");
            file = fileDialog.value.split('.');
            
            if(this.extensions.indexOf(file[file.length - 1]) >= 0) {
                this.picture = URL.createObjectURL(fileDialog.files[0]);
            } else {
                fileDialog.value = "";
            }
        },
        submit() {

        }
    }
})