from kivy.app import App
from kivy.clock import Clock
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.core.window import Window
from kivy.properties import ObjectProperty
from kivy.uix.textinput import TextInput
from kivy.uix.scrollview import ScrollView
from kivy.uix.button import Button
import socketio
import json


Window.size = (480, 600)


class Header(GridLayout):
    navbar = None
    is_navbar = False
    title = ObjectProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def show_menu(self):
        if not self.is_navbar:
            self.navbar.pos_hint = {'x': 0, 'y': 0}
        else:
            self.navbar.pos_hint = {'right': -1, 'y': 0}

        self.is_navbar = not self.is_navbar


class NavBar(BoxLayout):
    header = None
    header_title = None

    navbar_manager = None

    login_page = None
    feed_page = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def switch_screens(self, index, show_menu=True):
        print("Switch to " + str(index))

        self.messaging_button.background_normal = 'img/header/navbar_button.png'
        self.feed_button.background_normal = 'img/header/navbar_button.png'
        self.my_account_button.background_normal = 'img/header/navbar_button.png'
        self.find_button.background_normal = 'img/header/navbar_button.png'
        self.activity_button.background_normal = 'img/header/navbar_button.png'
        self.settings_button.background_normal = 'img/header/navbar_button.png'

        self.messaging_button.color = 'black'
        self.feed_button.color = 'black'
        self.my_account_button.color = 'black'
        self.find_button.color = 'black'
        self.activity_button.color = 'black'
        self.settings_button.color = 'black'

        if index == 0:
            self.header_title.text = 'E.N.B.L. - Messaging'

            self.messaging_button.background_normal = 'img/header/navbar_button_selected.png'
            self.messaging_button.color = 'white'
        elif index == 1:
            self.header_title.text = 'E.N.B.L. - Feed'

            self.feed_button.background_normal = 'img/header/navbar_button_selected.png'
            self.feed_button.color = 'white'

            self.navbar_manager.switch_to(self.navbar_manager.feed)
        elif index == 2:
            self.header_title.text = 'E.N.B.L. - usr123'

            self.my_account_button.background_normal = 'img/header/navbar_button_selected.png'
            self.my_account_button.color = 'white'

            self.navbar_manager.switch_to(Account('usr123', True), duration=0)
        elif index == 3:
            self.header_title.text = 'E.N.B.L. - Find'

            self.find_button.background_normal = 'img/header/navbar_button_selected.png'
            self.find_button.color = 'white'
        elif index == 4:
            self.header_title.text = 'E.N.B.L. - Activity'

            self.activity_button.background_normal = 'img/header/navbar_button_selected.png'
            self.activity_button.color = 'white'
        elif index == 5:
            self.header_title.text = 'E.N.B.L. - Settings'

            self.settings_button.background_normal = 'img/header/navbar_button_selected.png'
            self.settings_button.color = 'white'
        elif index == 6:
            self.header_title.text = 'E.N.B.L. - Sign in'

            self.login_page.is_login = True
            self.navbar_manager.switch_to(self.navbar_manager.login)

        if show_menu:
            self.header.show_menu()


class Login(Screen):
    is_login = True
    header_title = None
    nav_manager = None
    navbar = None
    socket_client = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def switch_logreg(self):
        self.is_login = not self.is_login

        if self.is_login:
            self.header_title.text = 'E.N.B.L. - Sign in'
        else:
            self.header_title.text = 'E.N.B.L. - Sign up'

    def login(self):
        # request = {
        #     'login': 'username1',
        #     'password': '123456'
        # }
        # self.socket_client.emit('auth', data=json.dumps(request))
        self.logged_in(True)

    def logged_in(self, is_logged_in):
        if is_logged_in:
            self.is_login = False
            self.navbar.switch_screens(1, show_menu=False)
        else:
            self.is_login = True
            print('User can\'t be logged in')


class Feed(Screen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class Account(Screen):
    def __init__(self, username, is_mine, **kwargs):
        super().__init__(**kwargs)
        self.username.text = username
        self.posts_block.bind(minimum_height=self.posts_block.setter('height'))


class NavManager(ScreenManager):
    pass


class MainContainer(FloatLayout):
    navbar = ObjectProperty()
    header = ObjectProperty()
    nav_manager = ObjectProperty()

    def __init__(self, client, **kwargs):
        super().__init__(**kwargs)

        self.nav_manager.transition.duration = 0

        self.header.navbar = self.navbar
        self.navbar.header = self.header
        self.navbar.header_title = self.header.title
        self.navbar.login_page = self.nav_manager.login
        self.navbar.feed_page = self.nav_manager.feed
        self.navbar.navbar_manager = self.nav_manager
        self.nav_manager.login.header_title = self.header.title
        self.nav_manager.login.navbar = self.navbar
        self.nav_manager.login.socket_client = client

        self.nav_manager.login.nav_manager = self.nav_manager

        if Window.size[0] <= 450:
            self.header.title.font_size = 25
        if Window.size[0] <= 300:
            self.header.title.font_size = 18
        if Window.size[0] <= 200:
            self.header.title.font_size = 14

        @client.on('auth')
        def on_auth(data):
            print(data)
            self.nav_manager.login.logged_in(True if json.loads(
                data)['code'] == 200 else False)


class MainApp(App):
    def build(self):
        client = socketio.Client()
        # client.connect('http://localhost:8080')

        @client.on('connect')
        def connect():
            print("Connected")

        return MainContainer(client)


if __name__ == "__main__":
    MainApp().run()
