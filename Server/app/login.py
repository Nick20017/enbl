from flask import jsonify

class Login:
    def __init__(self, client):
        users = client["users"]
        self.auth = users["auth"]

    def login(self, request):
        try:
            if "username" in request.form and "password" in request.form:
                user = self.auth.find_one({"username": request.form["username"], "password": request.form["password"]})
                if user is None:
                    response = {
                        "code": 400,
                        "message": "Incorrect login or password"
                    }
                else:
                    response = {
                        "code": 200,
                        "message": "User successfully logged in",
                        "user_id": user['user_id'] if 'user_id' in user else ""
                    }
            else:
                response = {
                    "code": 400,
                    "message": "Empty login or password"
                }
        except Exception as ex:
            response = {
                "code": 400,
                "message": str(ex)
            }
        return jsonify(response)