class Game:
    def __init__(self, mongoclient):
        game = mongoclient["game"]
        users = mongoclient["users"]
        self.auth = users["auth"]
        self.queue_users = game["queue_users"]
        self.matches = game["matches"]
        self.match_data = game["match_data"]

    def find_match(self, request):
        print(request.form)
        if "user_id" not in request.form:
            response = {
                "code": 400,
                "message": "ID must not be empty"
            }
            print(response)
            return response

        matches = list(self.matches.find())
        players = None
        for match in matches:
            if "players" not in match:
                continue
            if len(match["players"]) >= 10:
                continue
            if "match_id" not in match:
                continue
            match_id = match["match_id"]
            players = match["players"]
            players.append(request.form["user_id"])
            break

        if players is None:
            match = {
                "match_id": str(len(matches) + 1),
                "players": [request.form["user_id"]]
            }
            match_id = match["match_id"]
            match_data = {
                "match_id": match_id,
                "players": {
                    request.form["user_id"]: {
                        "hp": 100,
                        "ammo": 100,
                        "position": {
                            "x": 0,
                            "y": 0,
                            "z": 0
                        },
                        "rotation": {
                            "x": 0,
                            "y": 0,
                            "z": 0,
                            "w": 0
                        },
                        "shooting": []
                    }
                }
            }
            self.matches.insert_one(match)
            self.match_data.insert_one(match_data)
        else:
            self.matches.update_one({"match_id": match_id}, {"$set": {"players": players}})
            match_data = self.match_data.find_one({"match_id": match_id})
            if match_data is not None:
                if "players" in match_data:
                    players_data = match_data["players"]
                    players_data[request.form["user_id"]] = {
                        "hp": 100,
                        "ammo": 100,
                        "position": {
                            "x": 0,
                            "y": 0,
                            "z": 0
                        },
                        "rotation": {
                            "x": 0,
                            "y": 0,
                            "z": 0,
                            "w": 0
                        },
                        "shooting": []
                    }
                    self.match_data.update_one({"match_id": match_id}, {"$set": {"players": players_data}})

        response = {
            "code": 200,
            "message": "Room successfully found or created",
            "match_id": match_id
        }
        print(response)
        return response

    def exit_match(self, request):
        print(request.form)
        if "user_id" not in request.form or "match_id" not in request.form:
            response = {
                "code": 400,
                "message": "ID must not be empty"
            }
            print(response)
            return response

        match = self.matches.find_one({"match_id": request.form["match_id"]})
        match_data = self.match_data.find_one({"match_id": request.form["match_id"]})

        if match is None:
            response = {
                "code": 400,
                "message": "Match not found"
            }
            print(response)
            return response
            
        if "players" not in match:
            response = {
                "code": 200,
                "message": "Player already left the room"
            }
            print(response)
            return response

        if request.form["user_id"] not in match["players"]:
            response = {
                "code": 200,
                "message": "Player already left the room"
            }
            print(response)
            return response

        if match_data is not None:
            if "players" in match_data:
                players_data = match_data["players"]
                if request.form["user_id"] in players_data:
                    del players_data[request.form["user_id"]]
                    if len(players_data) == 0:
                        self.match_data.delete_one({"match_id": request.form["match_id"]})
                    else:
                        self.match_data.update_one({"match_id": request.form["match_id"]}, {"$set": {"players": players_data}})

        players = match["players"]
        players.remove(request.form["user_id"])
        if len(players) == 0:
            self.matches.delete_one({"match_id": match["match_id"]})
        else:
            self.matches.update_one({"match_id": match["match_id"]}, {"$set": {"players": players}})
        
        response = {
            "code": 200,
            "message": "Player successfully left the room"
        }
        print(response)
        return response

    def join_match(self, request):
        print(request.form)
        if "match_id" not in request.form or "user_id" not in request.form:
            response = {
                "code": 400,
                "message": "ID must not be empty"
            }
            print(response)
            return response
        
        match_id = request.form["match_id"]
        user_id = request.form["user_id"]
        match_data = self.match_data.find_one({"match_id": match_id})
        if match_data is None:
            response = {
                "code": 400,
                "message": "Match not found"
            }
            print(response)
            return response

        if "players" not in match_data:
            response = {
                "code": 400,
                "message": "Match not found"
            }
            print(response)
            return response

        players = match_data["players"]

        players[user_id]["hp"] = request.form["hp"]
        players[user_id]["ammo"] = request.form["ammo"]
        players[user_id]["position"] = {
            "x": request.form["position_x"],
            "y": request.form["position_y"],
            "z": request.form["position_z"]
        }
        players[user_id]["rotation"] = {
            "x": request.form["rotation_x"],
            "y": request.form["rotation_y"],
            "z": request.form["rotation_z"],
            "w": request.form["rotation_w"]
        }

        self.match_data.update_one({"match_id": match_id}, {"$set": {"players": players}})

        del players[user_id]
        
        response = {
            "code": 200,
            "players": players
        }
        print(response)
        return response

    def update_player_data(self, request):
        print(request.form)
        if "user_id" not in request.form or "match_id" not in request.form:
            response = {
                "code": 400,
                "message": "ID must not be empty"
            }
            print(response)
            return response
        
        match_id = request.form["match_id"]
        user_id = request.form["user_id"]

        match_data = self.match_data.find_one({"match_id": match_id})
        if match_data is None:
            response = {
                "code": 400,
                "message": "Match not found"
            }
            print(response)
            return response
        
        players = match_data["players"]
        if user_id not in players:
            response = {
                "code": 400,
                "message": "User is not in current match"
            }
            print(response)
            return response

        player_data = players[user_id]

        if "hp" in request.form:
            player_data["hp"] = request.form["hp"]
        if "ammo" in request.form:
            player_data["ammo"] = request.form["ammo"]
        if "position_x" in request.form:
            player_data["position"]["x"] = request.form["position_x"]
        if "position_y" in request.form:
            player_data["position"]["y"] = request.form["position_y"]
        if "position_z" in request.form:
            player_data["position"]["z"] = request.form["position_z"]
        if "rotation_x" in request.form:
            player_data["rotation"]["x"] = request.form["rotation_x"]
        if "rotation_y" in request.form:
            player_data["rotation"]["y"] = request.form["rotation_y"]
        if "rotation_z" in request.form:
            player_data["rotation"]["z"] = request.form["rotation_z"]
        if "rotation_w" in request.form:
            player_data["rotation"]["w"] = request.form["rotation_w"]
        if "shooting" in request.form:
            shooting = request.form["shooting"]
            shooting_arr = str.split(shooting, ',')
            if shooting == "":
                shooting_arr = []
            player_data["shooting"] = shooting_arr

        print(player_data)

        self.match_data.update_one({"match_id": match_id}, {"$set": {"players." + user_id: player_data}})

        response = {
            "code": 200,
            "message": "Data updated successfully"
        }
        print(response)
        return response

    def get_player_data(self, request):
        print(request.form)
        if "match_id" not in request.form or "user_id" not in request.form:
            response = {
                "code": 400,
                "message": "ID must not be empty"
            }
            print(response)
            return response

        match_data = self.match_data.find_one({"match_id": request.form["match_id"]})
        if "players" not in match_data:
            response = {
                "code": 400,
                "message": "Match not found"
            }
            print(response)
            return response

        players = match_data["players"]
        if request.form["user_id"] not in players:
            response = {
                "code": 400,
                "message": "Player not found"
            }
            print(response)
            return response

        del players[request.form["user_id"]]

        response = {
            "code": 200,
            "player_data": players
        }
        print(response)
        return response

    def find_players(self, request):
        print(request.form)
        if "match_id" not in request.form or "user_id" not in request.form:
            response = {
                "code": 400,
                "message": "ID must not be empty"
            }
            print(response)
            return response

        match_id = request.form["match_id"]
        user_id = request.form["user_id"]
        match = self.matches.find_one({"match_id": match_id})
        
        if match is None:
            response = {
                "code": 400,
                "message": "Match not found"
            }
            print(response)
            return response

        players = match["players"]
        players.remove(user_id)

        response = {
            "code": 200,
            "players": players
        }
        print(response)
        return response

    def shoot(self, request):
        print(request.form)
        if "match_id" not in request.form or "user_id" not in request.form:
            response = {
                "code": 400,
                "message": "ID must not be empty"
            }
            print(response)
            return response

        match_id = request.form["match_id"]
        user_id = request.form["user_id"]

        match_data = self.match_data.find_one({"match_id": match_id})
        if match_data is None:
            response = {
                "code": 400,
                "message": "Match not found"
            }
            print(response)
            return response

        players = match_data["players"]
        if user_id not in players:
            response = {
                "code": 400,
                "message": "User not in match"
            }
            return response

        player = players[user_id]

        player["shooting"].append("0")

        self.match_data.update_one({"match_id": match_id}, {"$set": {"players." + user_id: player}})

        response = {
            "code": 200,
            "message": "Player shot successfully"
        }
        print(response)
        return response