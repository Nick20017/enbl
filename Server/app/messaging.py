from datetime import datetime
import random

class Messaging:
    def __init__(self, client):
        users = client["users"]
        self.auth = users["auth"]
        self.messaging = users["messaging"]

    def send_message(self, request):
        try:
            if "sender_id" not in request.form or "target_id" not in request.form or "message" not in request.form or "type" not in request.form:
                response = {
                    "code": 400,
                    "message": "Data must not be empty"
                }
                return response

            sender_id = request.form["sender_id"]
            target_id = request.form["target_id"]
            _message = request.form["message"]
            _type = request.form["type"]

            target_messaging = self.messaging.find_one({"user_id": target_id})
            target_chats = target_messaging["chats"]
            if sender_id not in target_chats:
                target_chats[sender_id] = []
            _target_chat = target_chats[sender_id]

            sender_messaging = self.messaging.find_one({"user_id": sender_id})
            sender_chats = sender_messaging["chats"]
            if target_id not in sender_chats:
                sender_chats[target_id] = []
            _sender_chat = sender_chats[target_id]
            
            rand = random.Random()
            message_id = rand.randint(0, 1000000000)

            while True:
                match = False
                for msg in _target_chat:
                    if msg["message_id"] == message_id:
                        match = True
                        break
                if match:
                    message_id = rand.randint(0, 1000000000)
                else:
                    break

            dt = datetime.now()
            message_dt = str(dt.day) + "-" + str(dt.month) + "-" + str(dt.year) + " " + str(dt.hour) + ":" + str(dt.minute)

            target_message = {
                "message_id": message_id,
                "value": _message,
                "type": _type,
                "datetime": message_dt,
                "is_sender": False
            }

            sender_message = {
                "message_id": message_id,
                "value": _message,
                "type": _type,
                "datetime": message_dt,
                "is_sender": True
            }

            _target_chat.append(target_message)
            _sender_chat.append(sender_message)

            self.messaging.update_one({ "user_id": target_id }, { "$set": { "chats." + sender_id: _target_chat } })
            self.messaging.update_one({ "user_id": sender_id }, { "$set": { "chats." + target_id: _sender_chat } })

            response = {
                "code": 200,
                "message": "Message has been successfully sent"
            }
            return response
        except Exception as ex:
            print(ex)
            response = {
                "code": 400,
                "message": str(ex)
            }
            return response

    def get_chat_previews(self, request):
        if "user_id" not in request.form:
            response = {
                "code": 400,
                "message": "ID must not be empty"
            }
            return response
        
        user_id = request.form["user_id"]
        messaging = self.messaging.find_one({"user_id": user_id})

        if messaging is None:
            self.messaging.insert_one({"user_id": user_id, "chats": {}})
            response = {
                "code": 200,
                "username": username,
                "chat": []
            }
            return response

        chats = messaging["chats"]

        _chats = []
        for key in chats:
            chat = chats[key]
            username = self.auth.find_one({"user_id": key})
            if username is None:
                continue
            last_message = chat[-1]
            if last_message["type"] == "image":
                lm = "image"
            else:
                lm = last_message["value"]
            _chat = {
                "user_id": key,
                "username": username["username"],
                "last_message": lm
            }
            _chats.append(_chat)

        response = {
            "code": 200,
            "previews": _chats
        }
        return response

    def get_chat(self, request):
        if "current_user_id" not in request.form or "target_user_id" not in request.form:
            response = {
                "code": 400,
                "message": "ID must not be empty"
            }
            return response

        current_user_id = request.form["current_user_id"]
        target_user_id = request.form["target_user_id"]

        auth = self.auth.find_one({"user_id": target_user_id})
        if auth is None:
            response = {
                "code": 400,
                "message": "User not found"
            }
            return response

        username = auth["username"]

        messaging = self.messaging.find_one({"user_id": current_user_id})
        if messaging is None:
            self.messaging.insert_one({"user_id": current_user_id, "chats": {}})
            response = {
                "code": 200,
                "username": username,
                "chat": []
            }
            return response

        chats = messaging["chats"]
        if target_user_id not in chats:
            response = {
                "code": 200,
                "username": username,
                "chat": []
            }
            return response

        chat = chats[target_user_id]
        response = {
            "code": 200,
            "username": username,
            "chat": chat
        }
        return response