from flask import jsonify
import random
import time

class Register:
    def __init__(self, client):
        users = client["users"]
        self.auth = users["auth"]
        self.followers = users["followers"]
        self.followings = users["followings"]
        self.settings = users["settings"]
        self.activities = users["activities"]
        self.messaging = users["messaging"]

    def register(self, request):
        # try:
        print(request.form)
        if "username" in request.form and "password" in request.form:
            username = request.form['username']
            password = request.form['password']
            if username != "" and password != "":
                if self.auth.find_one({"username": username}) is not None:
                    response = {
                        "code": 400,
                        "message": "User already registered"
                    }
                else:
                    id = str(username[0:len(username) // 2:2]) + str(password[len(password) // 2: len(password):2]) + str(password[0:len(password) // 2:1]) + str(username[len(username) // 2:len(username):1])
                    while self.auth.find_one({"user_id": id}) is not None:
                        random.seed(time.time())
                        id = str(random.randint(10, 99)) + chr(random.randint(ord('a'), ord('z'))) + chr(random.randint(ord('a'), ord('z'))) + str(random.randint(10, 99)) + chr(random.randint(ord('A'), ord('Z'))) + chr(random.randint(ord('A'), ord('Z'))) + str(random.randint(10, 99))
                    print(request.form["profile_image"])
                    user = {
                        "user_id": id,
                        "username": username,
                        "password": password
                    }
                    followers = {
                        "user_id": id,
                        "followers": []
                    }
                    followings = {
                        "user_id": id,
                        "followings": []
                    }
                    settings = {
                        "user_id": id,
                        "profile_image": request.form["profile_image"],
                        "email": "",
                        "phone": ""
                    }
                    activities = {
                        "user_id": id,
                        "activities": []
                    }
                    messaging = {
                        "user_id": id,
                        "chats": {}
                    }
                    self.auth.insert_one(user)
                    self.followers.insert_one(followers)
                    self.followings.insert_one(followings)
                    self.settings.insert_one(settings)
                    self.activities.insert_one(activities)
                    self.messaging.insert_one(messaging)
                    print(user)
                    response = {
                        "code": 200,
                        "message": "User successfully registered",
                        "user_id": id
                    }
            else:
                response = {
                    "code": 400,
                    "message": "Empty login or password"
                }
        else:
            response = {
                "code": 400,
                "message": "Empty login or password"
            }
        # except Exception as ex:
        #     print(ex)
        #     response = {
        #         "code": 400,
        #         "message": str(ex)
        #     }

        print(response)
        return jsonify(response)