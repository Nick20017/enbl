from flask import Flask, request
import pymongo
from app.login import Login
from app.register import Register
from app.profile import Profile
from app.game import Game
from app.messaging import Messaging

app = Flask(__name__)

print("Connecting to MongoDB...")
client = pymongo.MongoClient("mongodb+srv://user:enbl-gaming-messenger@enbl.ts2a8.mongodb.net/users?retryWrites=true&w=majority")
print("Connected")

log = Login(client)
reg = Register(client)
profile = Profile(client)
game = Game(client)
messaging = Messaging(client)

@app.route('/')
def index():
    return '<h1>API works</h1>'

@app.route('/login', methods=['POST'])
def login():
    return log.login(request)

@app.route('/register', methods=['POST'])
def register():
    return reg.register(request)

@app.route('/get-profile', methods=['POST'])
def get_profile():
    return profile.get_profile(request)

@app.route('/get-profiles-by-username', methods=['POST'])
def get_profiles_by_username():
    return profile.get_profiles_by_username(request)

@app.route('/profile/follow', methods=['POST'])
def profile_follow():
    return profile.follow(request)

@app.route('/profile/unfollow', methods=['POST'])
def profile_unfollow():
    return profile.unfollow(request)

@app.route('/profile/is-following', methods=['POST'])
def is_following():
    return profile.is_following(request)

@app.route('/profile/get-follows', methods=['POST'])
def get_follows():
    return profile.get_follows(request)

@app.route('/profile/get-profile-image-bytes', methods=['POST'])
def get_profile_image_bytes():
    return profile.get_profile_image_bytes(request)

@app.route('/profile/add-activity', methods=['POST'])
def add_activity():
    return profile.add_activity(request)

@app.route('/profile/get-activities', methods=['POST'])
def get_activities():
    return profile.get_activities(request)

@app.route('/post/upload', methods=['POST'])
def upload_post():
    return profile.upload_post(request)

@app.route('/post/get-bytes', methods=['POST'])
def get_post_bytes():
    return profile.get_post_bytes(request)

@app.route('/messaging/send-message', methods=['POST'])
def send_message():
    return messaging.send_message(request)

@app.route('/messaging/get-chat-previews', methods=['POST'])
def get_chat_previews():
    return messaging.get_chat_previews(request)

@app.route('/messaging/get-chat', methods=['POST'])
def get_chat():
    return messaging.get_chat(request)

@app.route('/game/find-match', methods=['POST'])
def find_match():
    return game.find_match(request)

@app.route('/game/exit-match', methods=['POST'])
def exit_match():
    return game.exit_match(request)

@app.route('/game/join-match', methods=['POST'])
def join_match():
    return game.join_match(request)

@app.route('/game/update-player-data', methods=['POST'])
def update_player_data():
    return game.update_player_data(request)

@app.route('/game/get-player-data', methods=['POST'])
def get_player_data():
    return game.get_player_data(request)

@app.route('/game/find-players', methods=['POST'])
def find_players():
    return game.find_players(request)

@app.route('/game/shoot', methods=['POST'])
def shoot():
    return game.shoot(request)

if __name__ == "__main__":
    app.run(debug=True, threaded=True)