from flask import jsonify
import concurrent.futures
import datetime

class Profile:
    def __init__(self, client):
        users = client["users"]
        self.auth = users["auth"]
        self.posts = users["posts"]
        self.followers = users["followers"]
        self.followings = users["followings"]
        self.settings = users["settings"]
        self.activities = users["activities"]

    def get_profile(self, request):
        try:
            if "user_id" not in request.form:
                response = {
                    "code": 400,
                    "message": "ID must not be null!"
                }
            else:
                user_id = request.form['user_id']
                user = self.auth.find_one({"user_id": user_id})
                if user is None:
                    response = {
                        "code": 400,
                        "message": "User not found!"
                    }
                else:
                    if "username" in user:
                        username = user["username"]
                    else:
                        username = ""
                    profile_image = self.settings.find_one({"user_id": user_id})
                    if profile_image is None:
                        profile_image = ""
                    else:
                        if "profile_image" in profile_image:
                            profile_image = profile_image["profile_image"]
                        else:
                            profile_image = ""
                    posts_list = list(self.posts.find({"user_id": user_id}))
                    posts = []
                    for post in posts_list:
                        del post["_id"]
                        posts.append(post)
                    followers = self.followers.find_one({"user_id": user_id})
                    if followers is None:
                        num_followers = 0
                    else:
                        if "followers" in followers:
                            num_followers = len(followers["followers"])
                        else:
                            num_followers = 0
                    followings = self.followings.find_one({"user_id": user_id})
                    if followings is None:
                        num_followings = 0
                    else:
                        if "followings" in followings:
                            num_followings = len(followings["followings"])
                        else:
                            num_followings = 0
                    response = {
                        "code": 200,
                        "user_id": user_id,
                        "username": username,
                        "profile_image": profile_image,
                        "posts": posts,
                        "num_followers": num_followers,
                        "num_followings": num_followings
                    }
        except Exception as ex:
            response = {
                "code": 400,
                "message": ex
            }

        return jsonify(response)

    def get_profiles_by_username(self, request):
        if "username" not in request.form:
            response = {
                "code": 400,
                "message": "ID must not be null!"
            }
        else:
            self.username = request.form["username"]
            users = list(self.auth.find())
            self.found_users = []
            threads = min(len(users), 500)
            with concurrent.futures.ThreadPoolExecutor(max_workers=threads) as executor:
                executor.map(self.check_user, users)
            response = {
                "code": 200,
                "users": self.found_users
            }
        
        return jsonify(response)

    def check_user(self, user):
        if self.username in user["username"]:
            settings = self.settings.find_one({"user_id": user["user_id"]})
            if settings is None:
                profile_image = ""
            else:
                profile_image = settings['profile_image']
            self.found_users.append({
                "user_id": user["user_id"],
                "username": user["username"],
                "profile_image": profile_image
            })

    def get_follows(self, request):
        if "user_id" not in request.form:
            response = {
                "code": 400,
                "message": "ID must not be empty"
            }
            return response

        user_id = request.form["user_id"]
        follows = "followers"
        if "follows" in request.form:
            follows = request.form["follows"]
        if follows == "followers":
            follows_object = self.followers.find_one({"user_id": user_id})
        else:
            follows_object = self.followings.find_one({"user_id": user_id})
        if "followers" not in follows_object and "followings" not in follows_object:
            response = {
                "code": 400,
                "message": "An error occurred"
            }
            return response

        if follows == "followers":
            users = follows_object["followers"]
        else:
            users = follows_object["followings"]

        self.found_follows = []
        try:
            threads = min(len(users), 500)
            with concurrent.futures.ThreadPoolExecutor(max_workers=threads) as executor:
                executor.map(self.get_profile_by_id, users)
        except Exception as ex:
            pass

        response = {
            "code": 200,
            "users": self.found_follows
        }
        return jsonify(response)

    def get_profile_by_id(self, user_id):
        auth = self.auth.find_one({"user_id": user_id})
        if auth is None:
            return None
        if "username" not in auth:
            return None
        username = auth["username"]
        settings = self.settings.find_one({"user_id": user_id})
        if settings is None:
            profile_image = ""
        else:
            profile_image = settings["profile_image"]
        self.found_follows.append({
            "user_id": user_id,
            "username": username,
            "profile_image": profile_image
        })

    def follow(self, request):
        if "follower" not in request.form or "following" not in request.form:
            response = {
                "code": 400,
                "message": "ID must not be null!"
            }
        else:
            follower_id = request.form['follower']
            following_id = request.form['following']
            followers = self.followers.find_one({"user_id": following_id})
            del followers["_id"]
            if "followers" in followers:
                followers["followers"].append(follower_id)
            else:
                response = {
                    "code": 400,
                    "message": "An error occurred!"
                }
            followings = self.followings.find_one({"user_id": follower_id})
            del followings["_id"]
            if "followings" in followings:
                followings['followings'].append(following_id)
            else:
                response = {
                    "code": 400,
                    "message": "An error occurred!"
                }
            if followers is not None and followings is not None:
                self.followers.replace_one({'user_id': following_id}, followers)
                self.followings.replace_one({'user_id': follower_id}, followings)
                response = {
                    "code": 200,
                    "message": "User successfully folowed this account!"
                }
            else:
                response = {
                    "code": 400,
                    "message": "An error occurred!"
                }

        return jsonify(response)

    def unfollow(self, request):
        if "unfollower" not in request.form or "unfollowing" not in request.form:
            response = {
                "code": 400,
                "message": "ID must not be empty"
            }
        else:
            unfollower_id = request.form["unfollower"]
            unfollowing_id = request.form["unfollowing"]
            followers = self.followers.find_one({"user_id": unfollowing_id})
            del followers["_id"]
            if "followers" in followers:
                if unfollower_id in followers["followers"]:
                    followers["followers"].remove(unfollower_id)
            else:
                response = {
                    "code": 400,
                    "message": "An error occurred!"
                }
            followings = self.followings.find_one({"user_id": unfollower_id})
            del followings["_id"]
            if "followings" in followings:
                if unfollowing_id in followings["followings"]:
                    followings["followings"].remove(unfollowing_id)
            else:
                response = {
                    "code": 400,
                    "message": "An error occurred!"
                }
            if followers is not None and followings is not None:
                self.followers.replace_one({"user_id": unfollowing_id}, followers)
                self.followings.replace_one({"user_id": unfollower_id}, followings)
                response = {
                    "code": 200,
                    "message": "User successfully unfollowed this account!"
                }
                
        return jsonify(response)

    def is_following(self, request):
        if "current_user_id" not in request.form or "target_user_id" not in request.form:
            response = {
                "code": 400,
                "message": "ID must not be empty"
            }
            return response
        
        followers = self.followers.find_one({ "user_id": request.form["target_user_id"] })
        if "followers" not in followers:
            response = {
                "code": 400,
                "message": "An error occured"
            }
        
        response = {}
        response["code"] = 200
        if request.form["current_user_id"] in followers["followers"]:
            response["is_following"] = 1
        else:
            response["is_following"] = 0

        return jsonify(response)

    def upload_post(self, request):
        if "image" not in request.form:
            response = {
                "code": 400,
                "message": "File must not be empty"
            }
            return response

        if "user_id" not in request.form:
            response = {
                "code": 400,
                "message": "ID must not be empty"
            }
            return response

        user_id = request.form["user_id"]
        image = request.form["image"]
        if "extension" in request.form:
            extension = request.form["extension"]
        else:
            extension = "png"

        if "description" in request.form:
            description = request.form["description"]
        else:
            description = ""

        num_posts = len(list(self.posts.find({"user_id": user_id})))

        post_id = user_id + "_" + str(num_posts + 1)

        created_date = str(datetime.date.today())
        created_time = str(datetime.datetime.now().time)

        post_object = {
            "post_id": post_id,
            "user_id": user_id,
            "image_bytes": image,
            "extension": extension,
            "description": description,
            "created_date": created_date,
            "created_time": created_time
        }

        self.posts.insert_one(post_object)

        response = {
            "code": 200,
            "message": "File saved"
        }
        return response

    def get_post_bytes(self, request):
        if "post_id" not in request.form:
            response = {
                "code": 400,
                "message": "Post ID must not be empty"
            }
            return response

        post = self.posts.find_one({"post_id": request.form["post_id"]})
        if post is None:
            response = {
                "code": 400,
                "message": "Post not found"
            }
            return response

        response = {
            "code": 200,
            "bytes": post["image_bytes"]
        }
        return response

    def get_profile_image_bytes(self, request):
        if "user_id" not in request.form:
            response = {
                "code": 400,
                "message": "ID must not be empty"
            }
            return response

        settings = self.settings.find_one({"user_id": request.form["user_id"]})

        if "profile_image" not in settings:
            response = {
                "code": 400,
                "message": "User not found"
            }
            return response

        response = {
            "code": 200,
            "bytes": settings["profile_image"]
        }
        return response

    def add_activity(self, request):
        if "sender_id" not in request.form or "target_id" not in request.form or "type" not in request.form:
            response = {
                "code": 400,
                "message": "Data must not be empty"
            }
            return response

        post_id = None

        _type = request.form["type"]

        if _type == "post":
            if "post_id" not in request.form:
                response = {
                    "code": 400,
                    "message": "Post ID must not be empty"
                }
                return response
            post_id = request.form["post_id"]

        sender_id = request.form["sender_id"]
        target_id = request.form["target_id"]
        action = ""
        if _type == "post":
            action = "liked your post"
        elif _type == "follow":
            action = "started following you"
        elif _type == "comment":
            action = "commented on your post"
        elif _type == "mention":
            action = "mentioned you in a comment"
        elif _type == "tag":
            action = "tagged you on post"

        auth = self.auth.find_one({"user_id": sender_id})
        if auth is None:
            response = {
                "code": 400,
                "message": "User not found"
            }
            return response

        username = auth["username"]

        date = datetime.datetime.now().date()
        time = datetime.datetime.now().time()

        _date = str(date.day) + '.' + str(date.month) + '.' + str(date.year)
        _time = str(time.hour) + ':' + str(time.minute)

        activity = {
            "user_id": sender_id,
            "username": username,
            "text": action,
            "date": _date,
            "time": _time,
            "type": _type
        }

        if post_id is not None:
            activity["post_id"] = post_id

        activities = self.activities.find_one({"user_id": target_id})
        if activities is None:
            activities = {
                "user_id": target_id,
                "activities": [activity]
            }
            self.activities.insert_one(activities)
        else:
            activities = activities["activities"]
            activities.append(activity)
            self.activities.update_one({"user_id": target_id}, {"$set": {"activities": activities}})

        response = {
            "code": 200,
            "message": "Action successfully added"
        }
        return response
        

    def get_activities(self, request):
        if "user_id" not in request.form:
            response = {
                "code": 400,
                "message": "ID must not be empty"
            }
            return response

        user_id = request.form["user_id"]

        activities = self.activities.find_one({"user_id": user_id})
        if activities is None:
            response = {
                "code": 400,
                "message": "User not found"
            }
            return response

        response = {
            "code": 200,
            "activities": activities["activities"]
        }
        print(response)
        return response